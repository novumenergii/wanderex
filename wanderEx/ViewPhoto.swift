//
//  viewPhoto.swift
//  Wanderlist
//
//  Created by Teyon Cooper on 6/2/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//


import UIKit
import AVFoundation
import Firebase




class ViewPhoto: UIViewController {
    
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    
    var image: UIImage!
    var textViewPlaceholderText = "hello."

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photo.image = self.image
        
        captionTextView.text = textViewPlaceholderText
        captionTextView.delegate = self as? UITextViewDelegate
        
    }
    
    
    @IBAction func cancelPhoto(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        
        
    }
    @IBAction func imageApproved(_ sender: Any) {
      
        // if image != nil - if there was a picture taken and the image is not empty.
        
        if captionTextView.text != "" && captionTextView.text != textViewPlaceholderText && image != nil {
                let newPost = Post(image: image, caption: captionTextView.text)
                newPost.save()
                self.performSegue(withIdentifier: "photoAcceptedMVC", sender: nil)
        
            
        
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender:Any?) {
        if segue.identifier == "photoAcceptedMVC" {
            let mapViewVC = segue.destination as! ViewController
            mapViewVC.image = self.image
            
        }
    
        func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
       
    }
}




