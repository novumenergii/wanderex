//
//  newAccountSignupView.swift
//  wanderEx
//
//  Created by Teyon Cooper on 6/12/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//
import UIKit
import AVFoundation
import AVKit
import Firebase
import FirebaseAuth


class newAccountSignupView: UIViewController {

    @IBOutlet weak var signupVideoView: UIView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var wanderExploreShare: UILabel!
    
    
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(newAccountSignupView.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    

    @IBAction func createAccountSignupClicked(_ sender: Any) {
        guard let username = username.text else {return}
        guard let email = email.text else {return}
        guard let pass = password.text else {return}
        
        Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
            
            if error != nil {
            print(error!)
                
            }else{
                //success(callback representing a successful registration, and is followed by allowing the user to be move to the globeView.
                print("Signup Successful")
                // self in front of method. looks for method to perform segue inside the current class. self = class (newAccount) self calls on the entire class to be segued.
                
                self.performSegue(withIdentifier: "goToGlobeView", sender: self)
                
                
            
                    }
                }
       
            }
    
    
    
    
}
