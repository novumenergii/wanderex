
//  Post.swift
//  wanderEx
//
//  Created by Teyon Cooper on 6/17/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase



class Post {
    
    
    var imageDownloadURL: String?
    var caption: String?
    private var image: UIImage!
    private var photo:UIImageView!

    
    init(image: UIImage, caption: String){
        
        self.image = image
        self.caption = caption
       
        }

    
    func save(){
        
        let ref = Database.database().reference().child("photoPost").childByAutoId()
        let newPostKey = ref.key
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        
        let imageStorageRef = storageRef.child("images")
        
        let newImageStorageRef = imageStorageRef.child(newPostKey)
        
        newImageStorageRef.downloadURL(completion: { (url, error) in
            if (error == nil) {
                if let downloadURL = url {
                    //make download string
                    let downloadString = downloadURL.absoluteString
                }
            }else{
                
                }
            }
            
                        //error occured
            
                    )}
    
        
}
