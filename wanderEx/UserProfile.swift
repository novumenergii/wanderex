//
//  UserProfile.swift
//  Wanderlist
//
//  Created by Teyon Cooper on 6/7/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//

import MapKit
import CoreLocation
import UIKit
import FirebaseDatabase
import Firebase
import FirebaseStorage

class UserProfile: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
  @IBOutlet weak var stackView: UIStackView!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var tableView: UITableView!

    
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 19900000
    var coordinate2D = CLLocationCoordinate2DMake(20.874831, -110.809183)
    
    func updateMapRegion(rangeSpan:CLLocationDistance) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate2D, rangeSpan, rangeSpan)
        mapView.region = region
        
       
        }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mapView.delegate = self
        updateMapRegion(rangeSpan: regionRadius)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            self.performSegue(withIdentifier: "swipeBackToCamera", sender: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        _ = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        
        mapView.showsUserLocation = true
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if (view.annotation as? MKUserLocation) != nil {
            self.performSegue(withIdentifier: "moveToCamera", sender: nil)
            
        }
        
    }
}


