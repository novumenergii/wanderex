//
//  ViewController.swift
//  Wanderlink
//
//  Created by Teyon Cooper on 6/2/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase
import FirebaseAuth

class ViewController: UIViewController, MKMapViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate {
    
   
    @IBOutlet weak var mapView: MKMapView!
    
    
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 19900050
    var coordinate2D = CLLocationCoordinate2DMake(20.874831, -100.809183)
    var sharedImagePin = MKPointAnnotation()
    
    let sharedImageCoordinates = MKUserLocation()
    var image: UIImage!
    
    
    
    func updateMapRegion(rangeSpan:CLLocationDistance) {
        let region = MKCoordinateRegionMakeWithDistance(coordinate2D, rangeSpan, rangeSpan)
        mapView.region = region
        
        
    }
  
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mapView.delegate = self
        updateMapRegion(rangeSpan: regionRadius)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        
        
}
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        _ = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        
        
        sharedImagePin.coordinate = location.coordinate
        mapView.addAnnotation(sharedImagePin)
        
        
        
        mapView.showsUserLocation = true
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if (view.annotation as? MKUserLocation) != nil {
            self.performSegue(withIdentifier: "moveToCamera", sender: nil)
            
            }
        }
    @IBAction func logoutButtonPressed(_ sender: Any) {
        // throws means that this action has the potential to throwback an errors. errors range from no internet connection, no active network, etc. Method can throwout an error code.
        //enclose the code with the potential for error inside a "do" block.
        
        do{
           try Auth.auth().signOut()
            
            navigationController?.popToRootViewController(animated: true)
        }
        catch{
            print("error, theres a problem signing out")
        }
        
    }
    
    }
