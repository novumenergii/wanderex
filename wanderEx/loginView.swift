//
//  RootController.swift
//  wanderEx
//
//  Created by Teyon Cooper on 6/11/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//

import UIKit

import FirebaseAuth
import Firebase


class loginView: UIViewController, UITextFieldDelegate {
    
   
    @IBOutlet weak var loginScreen: UIView!
    
    @IBOutlet weak var wanderEX: UILabel!
    @IBOutlet weak var emailUsername: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var LoginButton: UIButton!

    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loginView.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
   
    @IBAction func loginButtonTouched(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailUsername.text!, password: password.text!) { (user, error) in
            // there was an error. if error != nil
            if error != nil {
                print(error!)
            }
            else{
                // authentication process successful
                print("login successful")
               self.performSegue(withIdentifier: "goToGView", sender: self)
        }
            
    }
}

    
    
}

