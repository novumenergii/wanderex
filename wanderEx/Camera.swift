//
//  Camera.swift
//  Wanderlink
//
//  Created by Teyon Cooper on 6/2/18.
//  Copyright © 2018 Teyon Cooper. All rights reserved.
//

import UIKit
import AVFoundation

class Camera : UIViewController {
    
    var captureSession = AVCaptureSession()
    var backFacingCamera: AVCaptureDevice?
    var frontFacingCamera: AVCaptureDevice?
    var currentCamera:AVCaptureDevice?
    
    var photoOutput: AVCapturePhotoOutput?
    var stillImage: UIImage?
    
    //camera preview Layer
    var cameraPreviewLayer : AVCaptureVideoPreviewLayer?
    
    var image: UIImage?
    

    //doubletap to toggle between cameras
    
    var toggleCameraGestureRecognizer = UITapGestureRecognizer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture(swipe: )))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backFacingCamera = device
            }else if device.position == AVCaptureDevice.Position.front{
                frontFacingCamera = device
                
            }
        }
        
        //default device
        currentCamera = backFacingCamera
        
        //config. the session with the output for capturing out still image
        
        photoOutput = AVCapturePhotoOutput()
        if #available(iOS 11.0, *) {
            photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
        
        do{
            let capturedDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(capturedDeviceInput)
            captureSession.addOutput(photoOutput!)
            
            
            cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
            cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            cameraPreviewLayer?.frame = self.view.frame
            cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
            
            
            
            
            captureSession.startRunning()
            
            toggleCameraGestureRecognizer.numberOfTapsRequired = 2
            toggleCameraGestureRecognizer.addTarget(self, action: #selector(toggleCamera))
            view.addGestureRecognizer(toggleCameraGestureRecognizer)
            
        }catch{
            print(error)
        }
        
    }
    
    @objc private func toggleCamera() {
        captureSession.beginConfiguration()
        let newDevice = (currentCamera?.position == . back) ?
            frontFacingCamera : backFacingCamera
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureDeviceInput)
        }
        
        let cameraInput: AVCaptureDeviceInput
        do{
            cameraInput = try AVCaptureDeviceInput(device: newDevice!)
            
        }catch{
            print(error)
            return
            
        }
        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }
        
        currentCamera = newDevice
        captureSession.commitConfiguration()
       
    }
    
    @objc func handleGesture(swipe: UISwipeGestureRecognizer) -> Void {
        switch swipe.direction.rawValue {
        case 2:
            performSegue(withIdentifier: "swipeToProfile", sender: self)
        default:
            break
        }
    }


@IBAction func cameraCaptureButton(_ sender: Any) {
    
    let settings = AVCapturePhotoSettings()
    photoOutput?.capturePhoto(with: settings, delegate: self)
    
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender:Any?) {
        if segue.identifier == "imageCaptureComplete" {
            let viewPhotoVC = segue.destination as! ViewPhoto
            viewPhotoVC.image = self.image
        }
        
    }
}
    
extension Camera: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?){
        if let imageData = photo.fileDataRepresentation() {
            print(imageData)
            image = UIImage(data: imageData)
            performSegue(withIdentifier: "captureComplete", sender: nil)
            
            }
        }
    }


